package adapters;

import com.martianlab.petsname.R;

import android.util.Log;
import android.view.View.OnClickListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AlphabetAdapter extends BaseAdapter {  
	
	private static String TAG = "AlphabetAdapter";
	
	private Context _context;
	private String[] _alphabet;  
	
	private AlphabetInterface iface;
	
    public AlphabetAdapter(Context _MyContext, String alphabet ) {
    	this._context = _MyContext;
    	_alphabet = alphabet.split(",");
    	iface = (AlphabetInterface) _MyContext;
    }
	      
    @Override
    public int getCount() {
    	return _alphabet.length;
    }
    
	@Override
	public View getView(int position, View inView, ViewGroup parent) {
		Log.d(TAG, "getView ===================================");
		View v = inView;
	         
	    if( v == null ){
	        LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        v = inflater.inflate(R.layout.item_alphabet, null);
	    }
	    
	    final String letter = _alphabet[position];
	    ((TextView) v).setText( letter );
	    
	    v.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		iface.onSelectLetter( letter );
        	}
        });
	    
	    return v;
    }
	
	@Override
	public Object getItem(int arg0) {
		//Log.d( TAG, "getItem" );
	    return _alphabet[arg0];
	}

    @Override
    public long getItemId(int arg0) {
       // TODO Auto-generated method stub
       // Log.d( TAG, "getItemId" );	 
       return 0;
    }

}