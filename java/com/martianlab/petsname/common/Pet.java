package com.martianlab.petsname.common;

public class Pet {
	private String name;
	private String gender;
	private String first_letter;
	private String family;
	private String lang;
	
	public Pet(){
		name = "noname";
		gender = "N/A";
		first_letter = "A";
		family = "dog";
		lang = "rus";
	}
	
	public Pet(String _name, String _gender, String _first_letter, String _family, String _lang){
		name = _name;
		gender = _gender;
		first_letter = _first_letter;
		family = _family;
		lang = _lang;
	}

	public String getName(){
		return name;
	}
	
	public String getFamily(){
		return family;
	}
	
	
	public String getLang(){
		return lang;
	}
	
	public String getFirstLetter(){
		return first_letter;
	}
	
	public String getGender(){
		return gender;
	}
	
	public Pet clone(){
		return new Pet( name, gender, first_letter, family, lang );
	}
	
}
