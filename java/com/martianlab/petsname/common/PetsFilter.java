package com.martianlab.petsname.common;

public class PetsFilter {
	private String _gender;
	private String _firstLetter;
	private String _lang;
	private String _family;
	
	public PetsFilter(){
		_gender = "";
		_firstLetter = "";
		_lang = "rus";
		_family = "";		
	}
	
	public void setFamily(String f){
		_family = f;
	}
	
	public String getFamily(){
		return _family;
	}
	
	public void setGender(String g){
		_gender = g;
	}
	
	public String getGender(){
		return _gender;
	}
	
	public void setLang(String l){
		_lang = l;
	}
	
	public String getLang(){
		return _lang;
	}
	
	public void setFirstLetter( String fl ){
		_firstLetter = fl;
	}
	
	public String getFirstLetter(){
		return _firstLetter;
	}
	
}
