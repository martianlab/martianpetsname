package com.martianlab.petsname.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

import com.martianlab.petsname.R;
import com.martianlab.petsname.R.xml;

public class PetsList {
	
	private final static String TAG = "PetList";
	
	private ArrayList<Pet> pets;
	
	public PetsList( Resources res ){
		pets = new ArrayList<Pet>();
		loadXMLResources(res.getXml(R.xml.names));
	}
	
	private void loadXMLResources( XmlResourceParser xrp ){
		try {

	        int eventType = xrp.getEventType();
	        while (eventType != XmlPullParser.END_DOCUMENT) {
	            if(eventType == XmlPullParser.START_TAG) {
	                if(xrp.getName().equals("names") ) {
	                    loadNames(xrp);
	                }
	            }
	            eventType = xrp.next();  
	        }
		}catch(IOException ioe) {
	        Log.e(TAG, ioe.getStackTrace().toString());
		}catch(XmlPullParserException xppe) {
	        Log.e(TAG, xppe.getStackTrace().toString());
		}finally{ 
	        xrp.close();
		}
		Log.d(TAG, String.valueOf(pets.size()));
	}
	
	private void loadNames( XmlResourceParser xrp ){
        String firstLetter="";
		String list = ""; 
        String gender = "";
        String family = "";
        String lang = "";
    	for(int i=0; i<xrp.getAttributeCount(); i++) {
    		if( xrp.getAttributeName(i).equals("letter") ){
    			firstLetter = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("pets") ){
    			family = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("gender") ){
        		gender = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("list") ){
        		list = xrp.getAttributeValue(i);
    		} else if( xrp.getAttributeName(i).equals("lang") ){
        		lang = xrp.getAttributeValue(i);
    		}
        }
    	
    	String[] names = list.split(",");
    	for( String name : names ){
    		pets.add(new Pet(name, gender, firstLetter, family, lang));
    	}
	}
	
	public ArrayList<Pet> getFilteredList( PetsFilter filter ){
		ArrayList<Pet> res = new ArrayList<Pet>();
		for( Pet p : pets ){
			Pet newPet = p.clone();
			boolean flag = true; 
			if( !filter.getFamily().equals("") ){
				if( !newPet.getFamily().equals( filter.getFamily() ) ){
					flag = false; 
				}
			}
			if( !filter.getGender().equals("") ){
				if( !newPet.getGender().equals( filter.getGender() ) ){
					flag = false; 
				}
			}
			if( !filter.getLang().equals("") ){
				if( !newPet.getLang().equals( filter.getLang() ) ){
					flag = false; 
				}
			}
			if( !filter.getFirstLetter().equals("") ){
				if( !newPet.getFirstLetter().equals( filter.getFirstLetter() ) ){
					flag = false; 
				}
			}			
			
			if( flag ){
				res.add( p.clone() );
			}
		}
		return res;
	}
	
	public ArrayList<Pet> filterByFamily( ArrayList<Pet> plist, String family ){
		ArrayList<Pet> res = new ArrayList<Pet>();
		for( Pet p : plist ){
			if( p.getFamily().equalsIgnoreCase(family) ){
				res.add( p.clone() );
			}
		}
		return res;
	}		
	
	public ArrayList<Pet> filterByLang( ArrayList<Pet> plist, String lang ){
		ArrayList<Pet> res = new ArrayList<Pet>();
		for( Pet p : plist ){
			if( p.getLang().equalsIgnoreCase(lang) ){
				res.add( p.clone() );
			}
		}
		return res;
	}	
	
	public ArrayList<Pet> filterByGender( ArrayList<Pet> plist, String gender ){
		ArrayList<Pet> res = new ArrayList<Pet>();
		for( Pet p : plist ){
			if( p.getGender().equalsIgnoreCase(gender) ){
				res.add( p.clone() );
			}
		}
		return res;
	}
	
	public ArrayList<Pet> filterByFirstLetter( ArrayList<Pet> plist, String firstLetter ){
		ArrayList<Pet> res = new ArrayList<Pet>();
		for( Pet p : plist ){
			if( p.getFirstLetter().equalsIgnoreCase(firstLetter) ){
				res.add( p.clone() );
			}
		}
		return res;
	}	
	
	public Pet getRandomPet(ArrayList<Pet> plist){
		Random r = new Random();
		return plist.get( r.nextInt(plist.size()-1)).clone();
	}
	
}
