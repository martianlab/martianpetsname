package com.martianlab.petsname.common;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class SerifTextView extends TextView {

    public SerifTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public SerifTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SerifTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/timesbi.ttf");
            setTypeface(tf);
        }
    }

}
