package com.martianlab.petsname.common;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class BannerTextView extends TextView {

    public BannerTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public BannerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BannerTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/banner_font_bold.ttf");
            setTypeface(tf);
        }
    }

}
