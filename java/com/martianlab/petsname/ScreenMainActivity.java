package com.martianlab.petsname;

import java.util.ArrayList;

import com.martianlab.petsname.common.Pet;
import com.martianlab.petsname.common.PetsFilter;
import com.martianlab.petsname.common.PetsList;

// 
// � ��� � ���� �������� ��� ����� ����� �������� �������, � �� �� ������ ��� ��� �������? � ���� �������� ������� ��� �������
// ���� ��������� "������ ������". ������ �������� ��� � ���, � ��������� ���� ��������� ��� ��������. � ���� �� ���� ������
// ����� 16000 ��������� ������.
//


import adapters.AlphabetAdapter;
import adapters.AlphabetInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Activity;
import android.content.Context;

public class ScreenMainActivity extends Activity implements AlphabetInterface {
	// https://play.google.com/store/apps/details?id=com.martianlab.petsname
	private static final String TAG = "ScreenMainActivity"; 
	
	private static final String RUSSIAN_ALPHABET = "�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�,�";
	private static final String ENGLISH_ALPHABET = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
	
	private static final int VIEW_SELECT_FAMILY = 0;
	private static final int VIEW_SELECT_GENDER = 1;
	private static final int VIEW_SELECT_FIRST_LETTER = 2;
	private static final int VIEW_RANDOM_NAME = 4;
	private static final int VIEW_LIST_NAMES = 5;
	
	private int selectedView;
	
	private View selectFamilyLayout;
	private View selectGenderLayout;
	private View selectLangLayout;
	private View selectFirstLetterLayout;
	private View ramdomNameLayout;
	private View nameListLayout;
	
	private GridView selectFirstLetterGridView;
	
	private Button selectFamilyDogButton;
	private Button selectFamilyCatButton;
	private Button selectGenderMaleButton;
	private Button selectGenderFemaleButton;
	private Button selectLangRusButton;
	private Button selectLangEngButton;	 
	
	private Button showFullListButton;
	private Button showRandomNameButton;
	
	private TextView randomNameTextView;
	
	private ArrayList<Pet> filteredNames;
	
	private PetsList pl;
	private PetsFilter petsFilter;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.screen_main);

		pl = new PetsList(this.getResources());		
		//filteredNames = pl.getFullList();
		petsFilter = new PetsFilter();
		
		selectFamilyLayout = (View) findViewById(R.id.screen_main_layout_select_family);
		selectGenderLayout = (View) findViewById(R.id.screen_main_layout_select_gender);
		selectLangLayout = (View) findViewById(R.id.screen_main_layout_select_lang);
		selectFirstLetterLayout = (View) findViewById(R.id.screen_main_layout_select_first_letter);
		ramdomNameLayout = (View) findViewById(R.id.screen_main_layout_random_name);
		nameListLayout = (View) findViewById(R.id.screen_main_layout_name_list);
		
		selectFamilyDogButton = (Button) findViewById(R.id.screen_main_button_select_family_dog);
		selectFamilyDogButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				// filteredNames = pl.filterByFamily(filteredNames, "dogs");
				petsFilter.setFamily("dogs");
				selectView( VIEW_SELECT_GENDER );
			}
		});
		 
		selectFamilyCatButton = (Button) findViewById(R.id.screen_main_button_select_family_cat);
		selectFamilyCatButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				//filteredNames = pl.filterByFamily(filteredNames, "cats");
				petsFilter.setFamily("cats");
				selectView( VIEW_SELECT_GENDER );
			}
		});		
		
		selectGenderMaleButton = (Button) findViewById(R.id.screen_main_button_select_gender_male);
		selectGenderMaleButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				//filteredNames = pl.filterByGender(filteredNames, "male");
				petsFilter.setGender("male");
				selectView( VIEW_SELECT_FIRST_LETTER );
			}
		});
		
		selectGenderFemaleButton = (Button) findViewById(R.id.screen_main_button_select_gender_female);
		selectGenderFemaleButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				//filteredNames = pl.filterByGender(filteredNames, "female");
				petsFilter.setGender("female");
				selectView( VIEW_SELECT_FIRST_LETTER );
			}
		});
		
		AlphabetAdapter alphabetAdapter = new AlphabetAdapter(this, RUSSIAN_ALPHABET);
		
		selectFirstLetterGridView = (GridView) findViewById(R.id.screen_main_gridview_alphabet);
		selectFirstLetterGridView.setAdapter(alphabetAdapter);
		
		selectLangRusButton = (Button) findViewById(R.id.screen_main_button_select_lang_rus);
		selectLangRusButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				//filteredNames = pl.filterByGender(filteredNames, "female");
				petsFilter.setGender("rus");
				selectView( VIEW_SELECT_FIRST_LETTER );
			}
		});
		
		selectLangEngButton = (Button) findViewById(R.id.screen_main_button_select_lang_eng);
		selectLangEngButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				//filteredNames = pl.filterByGender(filteredNames, "female");
				petsFilter.setGender("rus");
				selectView( VIEW_SELECT_FIRST_LETTER );
			}
		});
		
		showFullListButton = (Button) findViewById(R.id.screen_main_button_show_full_list);
		showFullListButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				showList();
			}
		});
		
		showRandomNameButton = (Button) findViewById(R.id.screen_main_button_show_random_name);
		showRandomNameButton.setOnClickListener( new OnClickListener(){
			@Override
			public void onClick(View arg0) {
				showRandomName();
			}
		});
		
		randomNameTextView = (TextView) findViewById(R.id.screen_main_textview_random_name);
		
		selectView( VIEW_SELECT_FAMILY );
		
	}
	
	@Override
	public void onBackPressed() {
		if( selectedView == 4 || selectedView == 5 ) {
			// ���� �� �� ������ ������ - ���� �� ������� �����
			petsFilter = new PetsFilter();
			selectView( VIEW_SELECT_FAMILY );
		} else if ( selectedView > 0 ){
			// ���� �� � �������� ������ ������� - ���� �� ����� �����
			selectView( selectedView - 1 ); 
		} else {
			// � ���� ������� ������� �� ����������
			super.onBackPressed();
		}
	}
	
	
	private void selectView( int view ){
		//Log.d(TAG, "Size = " + String.valueOf(filteredNames.size()));
		
		selectedView = view;
		
		selectFamilyLayout.setVisibility(View.GONE);
		selectGenderLayout.setVisibility(View.GONE);
		selectLangLayout.setVisibility(View.GONE);
		selectFirstLetterLayout.setVisibility(View.GONE);
		ramdomNameLayout.setVisibility(View.GONE);
		nameListLayout.setVisibility(View.GONE);
		
		switch(view){
			case VIEW_SELECT_FAMILY: selectFamilyLayout.setVisibility(View.VISIBLE); break;	
			case VIEW_SELECT_GENDER: selectGenderLayout.setVisibility(View.VISIBLE); break;  
			//case VIEW_SELECT_LANG: selectLangLayout.setVisibility(View.VISIBLE); break;
			case VIEW_SELECT_FIRST_LETTER: selectFirstLetterLayout.setVisibility(View.VISIBLE); break;
			case VIEW_RANDOM_NAME: ramdomNameLayout.setVisibility(View.VISIBLE); break;
			case VIEW_LIST_NAMES: nameListLayout.setVisibility(View.VISIBLE); 
		}
	} 
	
	private void showRandomName(){
		selectView( VIEW_RANDOM_NAME );
		if( filteredNames == null ){
			filteredNames = pl.getFilteredList(petsFilter);
		}
		
		Pet p = pl.getRandomPet( filteredNames );
		randomNameTextView.setText( p.getName() ); 
	}
	
	private void showList(){
		selectView( VIEW_LIST_NAMES );
		if( filteredNames == null ){
			filteredNames = pl.getFilteredList(petsFilter);
		}		 
		ListView listView = (ListView) findViewById(R.id.screen_main_listview_list_names);
		listView.setAdapter(new PetListAdapter(ScreenMainActivity.this, R.layout.row_pet, filteredNames));
		   //listView.setOnItemClickListener( new OnItemClickListener(){
		//		@Override
			//	public void onItemClick(AdapterView<?> l, View v, int position, long id) {
			//		selectAirport(l, position );
			//	}
		   //});		
	}
	
	/**
	 * ������� ������ ����� � ������
	 */
	@Override
	public void onSelectLetter(String letter) { 
		Log.d(TAG, letter);
		petsFilter.setFirstLetter( letter );
		showList();
	}
	
	
	/**
	 * ������� ��� ����������� ������ ���������� � ���� �����
	 * @author awacs
	 *
	 */
	private class PetListAdapter extends ArrayAdapter<Pet> {
		
		private ArrayList<Pet> _pets;
		
		private int viewId;
		
		public PetListAdapter(Context c, int textViewResourceId, ArrayList<Pet> litems) {
			super(c, textViewResourceId, litems);
			//this.context = c;
			this._pets = litems;
			this.viewId = textViewResourceId; 
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			LayoutInflater inflater=getLayoutInflater();
			View row=inflater.inflate(viewId, parent, false);
			TextView label = (TextView)row.findViewById(R.id.pet_row_label);
			label.setText(_pets.get(position).getName());
			return row;
		}
	}	
	
}
